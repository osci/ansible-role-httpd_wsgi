# Ansible role to manage WSGI installation for Apache Httpd

## Introduction

WSGI is used to run Python web applications.

This role installs the Apache Httpd module and configure your vhost to use WSGI.

This role also expects to work hand-in hand with the OSAS httpd role
(ansible-role-ah-httpd). You may call this role with the `httpd` role
variable for the vhost, or even simpler call it right after you setup
the vhost.

The vhost defined in `website_domain` would have this script associated to its root.
The vhost then needs to be defined (see _Adding a Vhost_ chapter in httpd role).

Here is a complete example:
```
- hosts: www
  vars:
    website_domain: www.example.com
  tasks:
    - name: Install web vhost
      include_role:
        name: httpd
        tasks_from: vhost
    - name: Install WSGI
      include_role:
        name: httpd_wsgi
      vars:
        wsgi_instance: myservice
        wsgi_script: "/usr/share/myservice/www/myservice.wsgi"
        wsgi_user: "myuser"
        wsgi_home: /var/www/www.example.com/app
```

## Variables

- **wsgi_instance**: short and unique service name (allowed characters whould match the `name` setting of the WSGIDaemonProcess directive)
- **wsgi_script**: script absolute path
- **wsgi_user**: user and group the script process will run as; the group must have the same name, and both must already exist
- **wsgi_home**: home of the application (working directory)
- **wsgi_threads**: number of threads allocated to the Python instance (defaults to 5)

## Server configuration

This feature use the `server` entrypoint.

Usually applications do not need to register their own signal handlers, but some may need it, like Mailman 3.
In this case you can set `wsgi_allow_app_signals` to True, which will ensure WSGIRestrictSignal is off.

```
- hosts: web
  tasks:
    - include_role:
        name: httpd_wsgi
        tasks_from: server
      vars:
        wsgi_allow_app_signals: True
```

